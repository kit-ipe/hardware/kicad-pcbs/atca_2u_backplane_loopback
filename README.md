# ATCA 2U BACKPLANE-LOOPBACK

Backplane design to fit thet 2U ATCA shelf from COMTEL with loopback connections for the TCDS2 from HUB1 and HUB2


## PCB design

Final thickness 1.6 mm

### Impedance calculation
- Layers 2 and 3: 100 Ohm stripline (W = 150 um, S = 200 um)

#### Polar
![PCB Routed](/images/Stripline_Polar.png)

#### Multi-CB
![PCB Routed](/images/Stripline_Multi.png)

#### Multek
![PCB Routed](/images/Stripline_Multek.png)


### Length matched differential pairs
- `*ETH*` signals are length matched to 87 and 62 mm

## 3D Views

### Top

![Top View 3D](/images/Top_3D.png)

### Bottom

![Bottom View 3D](/images/Bottom_3D.png)

## License

```
 ------------------------------------------------------------------------------
| Copyright Luis Ardila-Perez 2024.                                            |
|                                                                              |
| This source describes Open Hardware and is licensed under the CERN-OHL-S v2. |
|                                                                              |
| You may redistribute and modify this source and make products using it under |
| the terms of the CERN-OHL-S v2 (https://ohwr.org/cern_ohl_s_v2.txt).         |
|                                                                              |
| This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          |
| INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         |
| PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.  |
|                                                                              |
| Source location: https://gitlab.cern.ch/kit-ipe/cms-tt/hardware/             |
|                             kicad-pcbs/atca_2u_backplane_loopback            |
|                                                                              |
| As per CERN-OHL-S v2 section 4, should You produce hardware based on this    |
| source, You must where practicable maintain the Source Location visible      |
| on the external case of the Gizmo or other products you make using this      |
| source.                                                                      |
 ------------------------------------------------------------------------------
 ```
